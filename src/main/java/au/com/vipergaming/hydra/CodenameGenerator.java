package au.com.vipergaming.hydra;

import java.util.Random;

//Copied and modified from - https://github.com/Atrox/haikunatorjava
class CodenameGenerator {

    private static String[] adjectives = {
            "aged", "ancient", "autumn", "billowing", "bitter", "black", "blue", "bold",
            "broad", "broken", "calm", "cold", "cool", "crimson", "curly", "damp",
            "dark", "dawn", "delicate", "divine", "dry", "empty", "falling", "fancy",
            "flat", "floral", "fragrant", "frosty", "gentle", "green", "hidden", "holy",
            "icy", "jolly", "late", "lingering", "little", "lively", "long", "lucky",
            "misty", "morning", "muddy", "mute", "nameless", "noisy", "odd", "old",
            "orange", "patient", "plain", "polished", "proud", "purple", "quiet", "rapid",
            "raspy", "red", "restless", "rough", "round", "royal", "shiny", "shrill",
            "shy", "silent", "small", "snowy", "soft", "solitary", "sparkling", "spring",
            "square", "steep", "still", "summer", "super", "sweet", "throbbing", "tight",
            "tiny", "twilight", "wandering", "weathered", "white", "wild", "winter", "wispy",
            "withered", "yellow", "young"
    };

    private static String[] nouns = {
            "art", "band", "bar", "base", "bird", "block", "boat", "bonus",
            "bread", "breeze", "brook", "bush", "butterfly", "cake", "cell", "cherry",
            "cloud", "credit", "darkness", "dawn", "dew", "disk", "dream", "dust",
            "feather", "field", "fire", "firefly", "flower", "fog", "forest", "frog",
            "frost", "glade", "glitter", "grass", "hall", "hat", "haze", "heart",
            "hill", "king", "lab", "lake", "leaf", "limit", "math", "meadow",
            "mode", "moon", "morning", "mountain", "mouse", "mud", "night", "paper",
            "pine", "poetry", "pond", "queen", "rain", "recipe", "resonance", "rice",
            "river", "salad", "scene", "sea", "shadow", "shape", "silence", "sky",
            "smoke", "snow", "snowflake", "sound", "star", "sun", "sun", "sunset",
            "surf", "term", "thunder", "tooth", "tree", "truth", "union", "unit",
            "violet", "voice", "water", "water", "waterfall", "wave", "wildflower", "wind",
            "wood"
    };
    private static Random random = new Random();
    private static String delimiter = "-";
    private static String tokenChars = "0123456789";
    private static int tokenLength = 3;

    /**
     * Generate heroku-like random names
     *
     * @return String
     */
    public static String generateName() {

        String adjective = randomString(adjectives);
        String noun = randomString(nouns);

        StringBuilder token = new StringBuilder();
        if (tokenChars != null && tokenChars.length() > 0) {
            for (int i = 0; i < tokenLength; i++) {
                token.append(tokenChars.charAt(random.nextInt(tokenChars.length())));
            }
        }

        return adjective + delimiter + noun + delimiter + token.toString();
    }
    
    /**
     * Random string from string array
     *
     * @param s Array
     * @return String
     */
    private static String randomString(String[] s) {
        if (s == null || s.length <= 0) return "";
        return s[random.nextInt(s.length)];
    }
}
