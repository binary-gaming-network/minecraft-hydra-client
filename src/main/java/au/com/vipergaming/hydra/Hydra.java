package au.com.vipergaming.hydra;

import org.bukkit.plugin.java.JavaPlugin;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Logger;

/**
 * @author Jan Jaso - A Slav from the village.
 */
public class Hydra extends JavaPlugin {

    private static class HydraBackGroundSocket implements Runnable {
        private final Logger logger;
        private final String serverName;

        HydraBackGroundSocket(Logger log, String sName) {
            this.logger = log;
            this.serverName = sName;
        }

        @Override
        public void run() {
            try {
                var socket = new Socket("minecraft-proxy", 25599);// DNS of server running Hive on port 25599
                try {
                    PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
                    out.println(serverName);//Send Hive-Server our Hydra-Client server name or if no env var - auto generate it
                    out.flush();//Flush just in-case it did not auto-flush.
                    while(!socket.isOutputShutdown())
                    {
                        //Keep socket open - hive will send pings every 1 second.
                    }
                } catch (IOException e) {
                    logger.severe(e.getMessage());
                }
            } catch (IOException e) {
                logger.severe(e.getMessage());
            }
        }
    }

    @Override
    public void onEnable() {
        getLogger().info("Hail Hydra!");
        //Generate server name
        String serverName = CodenameGenerator.generateName();
        //try get the sys env var for the server name, if its there.
        //actually shouldnt need the try but no IDE to check.
        try {
            if(!System.getenv("SERVERNAME").isEmpty())
                serverName = System.getenv("SERVERNAME");
            else
                System.out.println("The system env SERVERNAME could not be found, using random server name.");
        }
        catch (Exception e)
        {
            System.out.println("Error occurred: The system env SERVERNAME could not be found, using random server name.");
        }

        ExecutorService pool = Executors.newFixedThreadPool(1);//1 thread only for listener
        pool.execute(new HydraBackGroundSocket(getLogger(), serverName));//Run listener in background so plugin doesn't stop game from starting

        getLogger().info(String.format("Hydra Client '%s' Running in Background - Continue Main Program", serverName));
    }

    @Override
    public void onDisable() {
        getLogger().info("Cut off one head, two more shall take its place!");
    }
}